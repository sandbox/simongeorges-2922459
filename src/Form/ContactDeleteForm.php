<?php

namespace Drupal\cdmfr\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Contact entities.
 *
 * @ingroup cdmfr
 */
class ContactDeleteForm extends ContentEntityDeleteForm {


}
